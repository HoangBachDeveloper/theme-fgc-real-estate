function doc1so(so) {
    var arr_chuhangdonvi = ['không', 'một', 'hai', 'ba', 'bốn', 'năm', 'sáu', 'bảy', 'tám', 'chín'];
    var resualt = '';
    resualt = arr_chuhangdonvi[so];
    return resualt;
}
function doc2so(so) {
    so = so.replace(' ', '');
    var arr_chubinhthuong = ['không', 'một', 'hai', 'ba', 'bốn', 'năm', 'sáu', 'bảy', 'tám', 'chín'];
    var arr_chuhangdonvi = ['mươi', 'mốt', 'hai', 'ba', 'bốn', 'lăm', 'sáu', 'bảy', 'tám', 'chín'];
    var arr_chuhangchuc = ['', 'mười', 'hai mươi', 'ba mươi', 'bốn mươi', 'năm mươi', 'sáu mươi', 'bảy mươi', 'tám mươi', 'chín mươi'];
    var resualt = '';
    var sohangchuc = so.substr(0, 1);
    var sohangdonvi = so.substr(1, 1);
    resualt += arr_chuhangchuc[sohangchuc];
    if (sohangchuc == 1 && sohangdonvi == 1)
        resualt += ' ' + arr_chubinhthuong[sohangdonvi];
    else if (sohangchuc == 1 && sohangdonvi > 1)
        resualt += ' ' + arr_chuhangdonvi[sohangdonvi];
    else if (sohangchuc > 1 && sohangdonvi > 0)
        resualt += ' ' + arr_chuhangdonvi[sohangdonvi];

    return resualt;
}
function doc3so(so) {
    var resualt = '';
    var arr_chubinhthuong = ['không', 'một', 'hai', 'ba', 'bốn', 'năm', 'sáu', 'bảy', 'tám', 'chín'];
    var sohangtram = so.substr(0, 1);
    var sohangchuc = so.substr(1, 1);
    var sohangdonvi = so.substr(2, 1);
    resualt = arr_chubinhthuong[sohangtram] + ' trăm';
    if (sohangchuc == 0 && sohangdonvi != 0)
        resualt += ' linh ' + arr_chubinhthuong[sohangdonvi];
    else if (sohangchuc != 0)
        resualt += ' ' + this.doc2so(sohangchuc + ' ' + sohangdonvi);
    return resualt;
}
function docsonguyen(so) {

    var result = '';
    if (so != undefined) {
        //alert(so);
        var arr_So = [{ ty: '' }, { trieu: '' }, { nghin: '' }, { tram: '' }];
        var sochuso = so.length;
        for (var i = (sochuso - 1); i >= 0; i--) {

            if ((sochuso - i) <= 3) {
                if (arr_So['tram'] != undefined)
                    arr_So['tram'] = so.substr(i, 1) + arr_So['tram'];
                else arr_So['tram'] = so.substr(i, 1);

            }
            else if ((sochuso - i) > 3 && (sochuso - i) <= 6) {
                if (arr_So['nghin'] != undefined)
                    arr_So['nghin'] = so.substr(i, 1) + arr_So['nghin'];
                else arr_So['nghin'] = so.substr(i, 1);
            }
            else if ((sochuso - i) > 6 && (sochuso - i) <= 9) {
                if (arr_So['trieu'] != undefined)
                    arr_So['trieu'] = so.substr(i, 1) + arr_So['trieu'];
                else arr_So['trieu'] = so.substr(i, 1);
            }
            else {
                if (arr_So.ty != undefined)
                    arr_So.ty = so.substr(i, 1) + arr_So.ty;
                else arr_So.ty = so.substr(i, 1);
            }
            //console.log(arr_So);
        }

        if (arr_So['ty'] > 0)
            result += this.doc(arr_So['ty']) + ' tỷ';
        if (arr_So['trieu'] > 0) {
            if (arr_So['trieu'].length >= 3 || arr_So['ty'] > 0)
                result += ' ' + this.doc3so(arr_So['trieu']) + ' triệu';
            else if (arr_So['trieu'].length >= 2)
                result += ' ' + this.doc2so(arr_So['trieu']) + ' triệu';
            else result += ' ' + this.doc1so(arr_So['trieu']) + ' triệu';
        }
        if (arr_So['nghin'] > 0) {
            if (arr_So['nghin'].length >= 3 || arr_So['trieu'] > 0)
                result += ' ' + this.doc3so(arr_So['nghin']) + ' nghìn';
            else if (arr_So['nghin'].length >= 2)
                result += ' ' + this.doc2so(arr_So['nghin']) + ' nghìn';
            else result += ' ' + this.doc1so(arr_So['nghin']) + ' nghìn';
        }
        if (arr_So['tram'] > 0) {
            if (arr_So['tram'].length >= 3 || arr_So['nghin'] > 0)
                result += ' ' + this.doc3so(arr_So['tram']);
            else if (arr_So['tram'].length >= 2)
                result += ' ' + this.doc2so(arr_So['tram']);
            else result += ' ' + this.doc1so(arr_So['tram']);
        }
    }
    return result;
}
function doc(so) {
    var kytuthapphan = ",";
    var result = '';
    if (so != undefined) {
        so = " " + so + " ";
        so = so.trim();
        var cautrucso = so.split(kytuthapphan);
        if (cautrucso[0] != undefined) {
            result += this.docsonguyen(cautrucso[0]);
        }
        if (cautrucso[1] != undefined) {
            //alert(this.docsonguyen(cautrucso[1]));
            result += ' phẩy ' + this.docsonguyen(cautrucso[1]);
        }
    }

    return result;
}
$(function () {


    $('.main-content__body-slide').slick(
        {
            arrows: false,
            autoplay: true,
            autoplaySpeed: 2000,
        }
    );

    $('.slide-ranger').ionRangeSlider({
        skin: "round",
        type: "double",
        min: 100000000,
        max: 10000000000,
        from: 500000000,
        to: 2000000000,
        grid: false,
        postfix: "VNĐ",
        step: 100000000
    });

    $('#categories-slider').ionRangeSlider({
        skin: "round",
        type: "double",
        min: 100000000,
        max: 10000000000,
        from: 500000000,
        to: 2000000000,
        grid: false,
        postfix: "VNĐ",
        step: 100000000
    });


    $('#price_min').text($('.irs-from').text())
    $('#price_max').text($('.irs-to').text())

    $('.slide-ranger').change(function (e) {
        e.preventDefault();
        $('#price_min').text($('.irs-from').text())
        $('#price_max').text($('.irs-to').text())
    });

    $('#categories-slider').change(function (e) {
        e.preventDefault();
        $('#price_min').text($('.irs-from').text())
        $('#price_max').text($('.irs-to').text())
    });



    // SLIDE PARTNER
    $('.partner-slide').slick({
        slidesToShow: 6,
        arrows: false,
        infinite: true,
        slidesToScroll: 6,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 5,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }

            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    // SLIDE ABOUT
    $('.about-img__large').slick({
        arrows: false,
        dots: true,
        autoplay: true
    });

    // SHOW PASSWORD
    $('#show-pass').click(function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
        let type = $('#password').attr('type');
        if (type == 'password') {
            $('#password').attr('type', 'text');
            $(this).val('Hide');
        }
        else {
            $('#password').attr('type', 'password');
            $(this).val('Show');
        }
    });

    // TOGGLE AUTH FORM
    $('#close-login').click(function (e) {
        e.preventDefault();
        $('#auth-main_form').removeClass('active');
        $('.auth-main__form--login').removeClass('active');
        $('.auth-main__form--register').removeClass('active');

    });

    $('#close-register').click(function (e) {
        e.preventDefault();
        $('#auth-main_form').removeClass('active');
        $('.auth-main__form--login').removeClass('active');
        $('.auth-main__form--register').removeClass('active');

    });

    $('#login').click(function (e) {
        e.preventDefault();
        $('#auth-main_form').addClass('active');
        $('.auth-main__form--login').addClass('active');

    });

    $('#register').click(function (e) {
        e.preventDefault();
        $('#auth-main_form').addClass('active');
        $('.auth-main__form--register').addClass('active');
    });

    $('#redirect-register').click(function (e) {
        e.preventDefault();
        $('.auth-main__form--login').removeClass('active');
        $('.auth-main__form--register').addClass('active');
    });

    $('#redirect-login').click(function (e) {
        e.preventDefault();
        $('.auth-main__form--register').removeClass('active');
        $('.auth-main__form--login').addClass('active');
    });



    // PAGINATION
    $('#pagination ul li').click(function (e) {
        e.preventDefault();
        $('#pagination ul li.active').removeClass('active');
        $(this).addClass('active');
    })

    // SELECT CLICK
    $('.form-group select').click(function (e) {
        $(this).toggleClass('active');
    });



});